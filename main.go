package main

import (
	"flag"
	"time"

	"gitlab.com/rodrigoodhin/gorec/pkg"
)

func main() {

	listOption := flag.Bool("l", false, "List option")
	flag.BoolVar(listOption, "list", false, "List option")

	cloneOption := flag.Bool("c", false, "Clone option")
	flag.BoolVar(cloneOption, "clone", false, "Clone option")

	privateToken := flag.String("t", "", "Private Token")
	url := flag.String("u", "", "Url")
	clonePath := flag.String("p", "./clones_"+time.Now().Format("20060102"), "Clone path")
	cloneGroup := flag.String("g", "", "Clone group")
	username := flag.String("n", "", "Username")

	flag.Parse()

	g := pkg.Gorec{
		PrivateToken: *privateToken,
		Url:          pkg.Protocol + *url,
		ClonePath:    *clonePath,
		CloneGroup:   *cloneGroup,
		User: pkg.User{
			Username: *username,
		},
	}

	g.ApiUrl = g.Url + pkg.ApiPath
	g.GroupsUrl = g.ApiUrl + pkg.GroupsPath
	g.ProjectsUrl = g.ApiUrl + pkg.ProjectsPath
	g.UsersUrl = g.ApiUrl + pkg.UsersPath

	if *listOption {
		g.ListProjects()
		g.ListGroups()
	}

	if *cloneOption {
		g.CloneProjects()
	}

}
