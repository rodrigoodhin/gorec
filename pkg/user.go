package pkg

import (
	"encoding/json"
	"fmt"
)

func (g *Gorec) getUserDetails() {
	resp, err := g.callApi(g.UsersUrl + "?username=" + g.User.Username + "&private_token=" + g.PrivateToken)
	if err != nil {
		fmt.Printf("error to get user id: %v\n", err)
	}

	users := []User{}
	err = json.Unmarshal([]byte(resp), &users)
	if err != nil {
		fmt.Printf("error unmarshalling user: %v\n", err)
	}

	g.User = users[0]
}
