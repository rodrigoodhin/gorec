package pkg

import "time"

const Protocol = "https://"
const GitlabUrl = "gitlab.com"
const ApiPath = "/api/v4"
const GroupsPath = "/groups"
const ProjectsPath = "/projects"
const UsersPath = "/users"

type Gorec struct {
	PrivateToken string
	Url          string
	ApiUrl       string
	GroupsUrl    string
	ProjectsUrl  string
	UsersUrl     string
	ClonePath    string
	CloneGroup   string
	User         User
	Projects     []Project
	Groups       []Group
}

type Project struct {
	Links struct {
		Events        string `json:"events"`
		Issues        string `json:"issues"`
		Labels        string `json:"labels"`
		Members       string `json:"members"`
		MergeRequests string `json:"merge_requests"`
		RepoBranches  string `json:"repo_branches"`
		Self          string `json:"self"`
	} `json:"_links"`
	AllowMergeOnSkippedPipeline interface{} `json:"allow_merge_on_skipped_pipeline"`
	AnalyticsAccessLevel        string      `json:"analytics_access_level"`
	Archived                    bool        `json:"archived"`
	AutoCancelPendingPipelines  string      `json:"auto_cancel_pending_pipelines"`
	AutoDevopsDeployStrategy    string      `json:"auto_devops_deploy_strategy"`
	AutoDevopsEnabled           bool        `json:"auto_devops_enabled"`
	AutocloseReferencedIssues   bool        `json:"autoclose_referenced_issues"`
	AvatarURL                   interface{} `json:"avatar_url"`
	BuildCoverageRegex          interface{} `json:"build_coverage_regex"`
	BuildTimeout                int         `json:"build_timeout"`
	BuildsAccessLevel           string      `json:"builds_access_level"`
	CanCreateMergeRequestIn     bool        `json:"can_create_merge_request_in"`
	CiConfigPath                string      `json:"ci_config_path"`
	CiDefaultGitDepth           int         `json:"ci_default_git_depth"`
	CiForwardDeploymentEnabled  bool        `json:"ci_forward_deployment_enabled"`
	CiJobTokenScopeEnabled      bool        `json:"ci_job_token_scope_enabled"`
	ContainerExpirationPolicy   struct {
		Cadence       string      `json:"cadence"`
		Enabled       bool        `json:"enabled"`
		KeepN         int         `json:"keep_n"`
		NameRegex     string      `json:"name_regex"`
		NameRegexKeep interface{} `json:"name_regex_keep"`
		NextRunAt     time.Time   `json:"next_run_at"`
		OlderThan     string      `json:"older_than"`
	} `json:"container_expiration_policy"`
	ContainerRegistryEnabled     bool        `json:"container_registry_enabled"`
	ContainerRegistryImagePrefix string      `json:"container_registry_image_prefix"`
	CreatedAt                    time.Time   `json:"created_at"`
	CreatorID                    int         `json:"creator_id"`
	DefaultBranch                string      `json:"default_branch"`
	Description                  string      `json:"description"`
	EmailsDisabled               interface{} `json:"emails_disabled"`
	EmptyRepo                    bool        `json:"empty_repo"`
	ForkingAccessLevel           string      `json:"forking_access_level"`
	ForksCount                   int         `json:"forks_count"`
	HTTPURLToRepo                string      `json:"http_url_to_repo"`
	ID                           int         `json:"id"`
	ImportStatus                 string      `json:"import_status"`
	IssuesAccessLevel            string      `json:"issues_access_level"`
	IssuesEnabled                bool        `json:"issues_enabled"`
	JobsEnabled                  bool        `json:"jobs_enabled"`
	KeepLatestArtifact           bool        `json:"keep_latest_artifact"`
	LastActivityAt               time.Time   `json:"last_activity_at"`
	LfsEnabled                   bool        `json:"lfs_enabled"`
	MergeMethod                  string      `json:"merge_method"`
	MergeRequestsAccessLevel     string      `json:"merge_requests_access_level"`
	MergeRequestsEnabled         bool        `json:"merge_requests_enabled"`
	Name                         string      `json:"name"`
	NameWithNamespace            string      `json:"name_with_namespace"`
	Namespace                    struct {
		AvatarURL interface{} `json:"avatar_url"`
		FullPath  string      `json:"full_path"`
		ID        int         `json:"id"`
		Kind      string      `json:"kind"`
		Name      string      `json:"name"`
		ParentID  int         `json:"parent_id"`
		Path      string      `json:"path"`
		WebURL    string      `json:"web_url"`
	} `json:"namespace"`
	OnlyAllowMergeIfAllDiscussionsAreResolved bool   `json:"only_allow_merge_if_all_discussions_are_resolved"`
	OnlyAllowMergeIfPipelineSucceeds          bool   `json:"only_allow_merge_if_pipeline_succeeds"`
	OpenIssuesCount                           int    `json:"open_issues_count"`
	OperationsAccessLevel                     string `json:"operations_access_level"`
	PackagesEnabled                           bool   `json:"packages_enabled"`
	PagesAccessLevel                          string `json:"pages_access_level"`
	Path                                      string `json:"path"`
	PathWithNamespace                         string `json:"path_with_namespace"`
	Permissions                               struct {
		GroupAccess struct {
			AccessLevel       int `json:"access_level"`
			NotificationLevel int `json:"notification_level"`
		} `json:"group_access"`
		ProjectAccess interface{} `json:"project_access"`
	} `json:"permissions"`
	PrintingMergeRequestLinkEnabled bool          `json:"printing_merge_request_link_enabled"`
	PublicJobs                      bool          `json:"public_jobs"`
	ReadmeURL                       interface{}   `json:"readme_url"`
	RemoveSourceBranchAfterMerge    bool          `json:"remove_source_branch_after_merge"`
	RepositoryAccessLevel           string        `json:"repository_access_level"`
	RequestAccessEnabled            bool          `json:"request_access_enabled"`
	ResolveOutdatedDiffDiscussions  bool          `json:"resolve_outdated_diff_discussions"`
	RestrictUserDefinedVariables    bool          `json:"restrict_user_defined_variables"`
	ServiceDeskAddress              interface{}   `json:"service_desk_address"`
	ServiceDeskEnabled              bool          `json:"service_desk_enabled"`
	SharedRunnersEnabled            bool          `json:"shared_runners_enabled"`
	SharedWithGroups                []interface{} `json:"shared_with_groups"`
	SnippetsAccessLevel             string        `json:"snippets_access_level"`
	SnippetsEnabled                 bool          `json:"snippets_enabled"`
	SquashOption                    string        `json:"squash_option"`
	SSHURLToRepo                    string        `json:"ssh_url_to_repo"`
	StarCount                       int           `json:"star_count"`
	Statistics                      struct {
		CommitCount      int64 `json:"commit_count"`
		JobArtifactsSize int64 `json:"job_artifacts_size"`
		LfsObjectsSize   int64 `json:"lfs_objects_size"`
		PackagesSize     int64 `json:"packages_size"`
		RepositorySize   int64 `json:"repository_size"`
		SnippetsSize     int64 `json:"snippets_size"`
		StorageSize      int64 `json:"storage_size"`
		WikiSize         int64 `json:"wiki_size"`
	} `json:"statistics"`
	SuggestionCommitMessage interface{}   `json:"suggestion_commit_message"`
	TagList                 []interface{} `json:"tag_list"`
	Topics                  []interface{} `json:"topics"`
	Visibility              string        `json:"visibility"`
	WebURL                  string        `json:"web_url"`
	WikiAccessLevel         string        `json:"wiki_access_level"`
	WikiEnabled             bool          `json:"wiki_enabled"`
}

type Group struct {
	AutoDevopsEnabled              interface{} `json:"auto_devops_enabled"`
	AvatarURL                      interface{} `json:"avatar_url"`
	CreatedAt                      time.Time   `json:"created_at"`
	DefaultBranchProtection        int         `json:"default_branch_protection"`
	Description                    string      `json:"description"`
	EmailsDisabled                 interface{} `json:"emails_disabled"`
	FullName                       string      `json:"full_name"`
	FullPath                       string      `json:"full_path"`
	ID                             int         `json:"id"`
	LfsEnabled                     bool        `json:"lfs_enabled"`
	MentionsDisabled               interface{} `json:"mentions_disabled"`
	Name                           string      `json:"name"`
	ParentID                       int         `json:"parent_id"`
	Path                           string      `json:"path"`
	ProjectCreationLevel           string      `json:"project_creation_level"`
	RequestAccessEnabled           bool        `json:"request_access_enabled"`
	RequireTwoFactorAuthentication bool        `json:"require_two_factor_authentication"`
	ShareWithGroupLock             bool        `json:"share_with_group_lock"`
	SubgroupCreationLevel          string      `json:"subgroup_creation_level"`
	TwoFactorGracePeriod           int         `json:"two_factor_grace_period"`
	Visibility                     string      `json:"visibility"`
	WebURL                         string      `json:"web_url"`
}

type User struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	Name      string `json:"name"`
	State     string `json:"state"`
	AvatarURL string `json:"avatar_url"`
	WebURL    string `json:"web_url"`
}
