package pkg

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strconv"

	"github.com/olekukonko/tablewriter"
)

func (g *Gorec) ListGroups() {
	if len(g.Groups) == 0 {
		g.loadGroups()
	}

	tableData := [][]string{}

	for _, r := range g.Groups {
		tableData = append(tableData, []string{strconv.Itoa(r.ID),r.Name,r.Path,r.WebURL})
	}
	
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Id", "Group", "Path", "Url"})
	table.SetAutoWrapText(false)
	table.SetBorder(true)

	for _, v := range tableData {
		table.Append(v)
	}
	table.Render()   
}

func (g *Gorec) loadGroups() (err error) {

	s := g.createSpinner("Loading Groups: ")
	s.Start()  

	for i := 1; i <= 1000; i++ {

		resp, err := g.getGroupsJson(i)
		if err != nil {
			fmt.Printf("error to get groups json: %v\n", err)
		}

		if resp == "[]" {
			break
		}

		groups := []Group{}
		err = json.Unmarshal([]byte(resp), &groups)
		if err != nil {
			fmt.Printf("error unmarshalling groups: %v\n", err)
		}

		for _, group := range groups {
			if group.ParentID == 0 {
				g.Groups = append(g.Groups, group)
			}
		}

	}

	sort.Slice(g.Groups[:], func(i, j int) bool {
		return g.Groups[i].ID < g.Groups[j].ID
	})
	
	s.Stop()

	return
}

func (g *Gorec) getGroupsJson(page int) (resp string, err error) {
	resp, err = g.callApi(g.GroupsUrl + "?private_token=" + g.PrivateToken + "&simple=yes&per_page=100&page=" + strconv.Itoa(page))
	if err != nil {
		fmt.Printf("error to get groups: %v\n", err)
	}

	return
}
