package pkg

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/olekukonko/tablewriter"

	"github.com/dustin/go-humanize"
	"github.com/go-git/go-git/v5"
)

func (g *Gorec) ListProjects() {
	if len(g.Projects) == 0 {
		g.loadProjects()
	}

	tableData := [][]string{}

	var countSize uint64
	countSize = 0

	for _, p := range g.Projects {
		countSize += uint64(p.Statistics.RepositorySize)

		tableData = append(tableData, []string{strconv.Itoa(p.ID),p.Name,p.SSHURLToRepo,humanize.Bytes(uint64(p.Statistics.RepositorySize))})
	}
	
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Id", "Project", "Ssh Url", "Size"})
	table.SetFooter([]string{"", "", "Total", humanize.Bytes(countSize)})
	table.SetAutoWrapText(false)
	table.SetBorder(true)

	for _, v := range tableData {
		table.Append(v)
	}
	table.Render()       
}

func (g *Gorec) CloneProjects() {
	if len(g.Projects) == 0 {
		g.loadProjects()
	}

	fmt.Printf("\n")
	fmt.Printf("+--------------------------------------------------+\n")
	fmt.Printf("|                 Cloning Projects                 |\n")
	fmt.Printf("+--------------------------------------------------+\n")

	w := tabwriter.NewWriter(os.Stdout, 1, 1, 1, ' ', 0)
	fmt.Fprintln(w, "| ID\t | NAME\t | OUTPUT\t |")

	for _, p := range g.Projects {
		if p.Namespace.FullPath == g.CloneGroup || strings.HasPrefix( p.Namespace.FullPath,g.CloneGroup+"/") {
			path := strings.ReplaceAll(p.SSHURLToRepo, "git@", "")
			path = strings.ReplaceAll(path, g.Url+":", "")
			path = strings.ReplaceAll(path, ".git", "")
			path = g.ClonePath + "/" + path
			path = strings.ReplaceAll(path, "//", "/")

			if _, err := os.Stat(path); !errors.Is(err, os.ErrNotExist) {
				err := g.removeContents(path)
				if err != nil {
					fmt.Printf("error to remove old project folder: %v\n", err)
				}
			}

			err := os.MkdirAll(path, os.ModePerm)
			if err != nil {
				fmt.Printf("error to create project clone folder: %v\n", err)
			}

			_, err = git.PlainClone(path, false, &git.CloneOptions{
				URL: p.SSHURLToRepo,
			})
			if err != nil {
				fmt.Printf("error to clone project %v: %v\n", p.Name, err)
			}

			fmt.Fprintln(w, "| "+strconv.Itoa(p.ID)+"\t | "+p.Name+"\t | Successfully Cloned\t |")
		}
	}

	w.Flush()
}

func (g *Gorec) loadProjects() (err error) {

	s := g.createSpinner("Loading Projects: ")
	s.Start()   

	for i := 1; i <= 1000; i++ {

		resp, err := g.getProjectsJson(i)
		if err != nil {
			fmt.Printf("error to get projects json: %v\n", err)
		}

		if resp == "[]" || resp == "" {
			break
		}

		projects := []Project{}
		err = json.Unmarshal([]byte(resp), &projects)
		if err != nil {
			fmt.Printf("error unmarshalling projects: %v\n", err)
		}

		g.Projects = append(g.Projects, projects...)

	}

	for _, p := range g.Projects {
		if p.Statistics.RepositorySize == 0 {
			project, err := g.getProjectDetails(p.ID)
			if err != nil {
				fmt.Printf("error to get project %v details: %v\n", p.Name, err)
			}

			p.Statistics = project.Statistics
		}
	}

	sort.Slice(g.Projects[:], func(i, j int) bool {
		return g.Projects[i].ID < g.Projects[j].ID
	})

	s.Stop()

	return
}

func (g *Gorec) getProjectsJson(page int) (resp string, err error) {
	if g.Url != Protocol+GitlabUrl {
		resp, err = g.callApi(g.ProjectsUrl + "?private_token=" + g.PrivateToken + "&statistics=true&per_page=100&page=" + strconv.Itoa(page))
		if err != nil {
			fmt.Printf("error to get projects: %v\n", err)
		}
	} else {
		g.getUserDetails()

		resp, err = g.callApi(g.UsersUrl + "/" + strconv.Itoa(g.User.ID) + ProjectsPath + "?private_token=" + g.PrivateToken + "&statistics=true&per_page=100&page=" + strconv.Itoa(page))
		if err != nil {
			fmt.Printf("error to get projects: %v\n", err)
		}
	}

	return
}
