package pkg

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/briandowns/spinner"
)

func (g *Gorec) callApi(url string) (sb string, err error) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("error calling api: %v\n", err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("error reading response body: %v\n", err)
	}

	sb = string(body)

	return
}

func (g *Gorec) removeContents(dir string) (err error) {
	d, err := os.Open(dir)
	if err != nil {
		fmt.Printf("error opening folder to remove content: %v\n", err)
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		fmt.Printf("error reading directory names to remove content: %v\n", err)
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			fmt.Printf("error removing all folder contents: %v\n", err)
		}
	}
	return nil
}

func (g *Gorec) getProjectDetails(projectId int) (project Project, err error) {
	resp, err := g.callApi(g.ProjectsUrl + "/" + strconv.Itoa(projectId) + "?private_token=" + g.PrivateToken + "&statistics=true")
	if err != nil {
		fmt.Printf("error to get project details: %v\n", err)
	}

	err = json.Unmarshal([]byte(resp), &project)
	if err != nil {
		fmt.Printf("error unmarshalling project: %v\n", err)
	}

	return
}

func (g *Gorec) createSpinner(prefix string) *spinner.Spinner{
	someSet := []string{"∙∙∙∙", "●∙∙∙", "∙●∙∙", "∙∙●∙", "∙∙∙●"}
	s := spinner.New(someSet, 100*time.Millisecond)
	s.Prefix = prefix
	return s
}
